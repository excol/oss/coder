use std::fs;
use std::path::Path;
use std::process::exit;

use clap::Clap;
use json5;

mod cmd;
mod config;
mod files;
mod git;
mod term;

use cmd::{list, run, test};
use config::Config;

#[derive(Clap)]
#[clap(version = "0.0.1", author = "Dillen Meijboom <info@excol.nl>")]
enum Cmd {
    /// Run all tests in the configuration file
    #[clap()]
    Test(test::Opts),
    /// List all scripts in the project
    #[clap(alias = "ls")]
    List(list::Opts),
    /// Run a script in the project
    #[clap()]
    Run(run::Opts),
}

fn main() {
    let mut config = Config::default();
    let config_path = Path::new(".coder");

    if config_path.exists() {
        let contents = fs::read_to_string(config_path).expect("failed to read config file");
        config = json5::from_str(&contents).expect("failed to parse config file");
    }

    let result = match Cmd::parse() {
        Cmd::Run(opts) => run::run(config, opts),
        Cmd::Test(opts) => test::run(config, opts),
        Cmd::List(opts) => list::run(config, opts),
    };

    if let Err(e) = result {
        eprintln!("error: {}", e);
        exit(1);
    }
}
