use std::process::{exit, Command, Output};
use std::str;

use anyhow::Result;
use clap::Clap;
use colored::Colorize;
use templar::{Context, Document, StandardContext, Templar};

use crate::config::{Config, Test};
use crate::files::{find, File};
use crate::git::{list_files, list_staged_files};
use crate::term;

#[derive(Clap)]
pub struct Opts {
    #[clap(long, short)]
    pub commit: bool,
    #[clap(long)]
    pub continue_on_error: bool,
}

fn render_cmd(test: &Test, file: Option<&File>) -> Result<String> {
    let template = Templar::global().parse(&test.cmd)?;

    let mut data: Document = Document::default();

    if let Some(file) = file {
        data["file"]["name"] = file.name.as_str().into();

        if let Some(oid) = &file.git_oid {
            data["file"]["git"]["oid"] = oid.into();
            data["file"]["cat"] = format!("git cat-file -p \"{}\"", oid).into();
        } else {
            data["file"]["cat"] = format!("cat \"{}\"", file.name).into();
        }
    }

    let context = StandardContext::new();

    context.set(data)?;

    Ok(template.render(&context)?)
}

fn run_sh_command(cmd: impl AsRef<str>) -> Result<Output> {
    Ok(Command::new("sh").arg("-c").arg(cmd.as_ref()).output()?)
}

fn run_test(files: Vec<&File>, test: &Test) -> Result<(Option<String>, Output)> {
    if !test.cmd.contains("{{file") && !test.cmd.contains("{{ file") {
        let cmd = render_cmd(test, None)?;

        return Ok((None, run_sh_command(cmd)?));
    }

    let mut stdout = vec![];
    let mut stderr = vec![];
    let mut status = None;
    let mut file_name = None;

    for file in files {
        let cmd = render_cmd(test, file.into())?;
        let mut output = run_sh_command(cmd)?;

        status = output.status.clone().into();
        file_name = file.name.clone().into();

        stdout.append(&mut output.stdout);
        stderr.append(&mut output.stderr);

        if !output.status.success() {
            break;
        }
    }

    return Ok((
        file_name,
        Output {
            status: status.unwrap(),
            stdout,
            stderr,
        },
    ));
}

pub fn run(config: Config, opts: Opts) -> Result<()> {
    let files = if opts.commit {
        list_staged_files()
    } else {
        list_files()
    }?;

    term::print_heading("tests");

    for test in config.tests.iter() {
        let matches = test.when.as_ref().and_then(|when| when.matches.clone());
        let test_files = match &matches {
            Some(glob) => find(files.iter(), &glob)?.collect::<Vec<_>>(),
            None => files.iter().collect::<Vec<_>>(),
        };

        if test_files.is_empty() && matches.is_some() {
            continue;
        }

        println!(".. {}", test.name);

        match run_test(test_files, &test) {
            Ok((file_name, output)) => {
                term::erase_previous_line();

                if output.status.success() {
                    term::print_success(&test.name);
                    continue;
                }

                if let Some(name) = file_name {
                    term::print_error(format!(
                        "{} {}",
                        test.name,
                        format!("(at: {})", name.underline()).yellow(),
                    ));
                } else {
                    term::print_error(&test.name);
                }

                if !output.stdout.is_empty() {
                    println!(
                        "{}",
                        str::from_utf8(&output.stdout)?.trim_end().bright_black(),
                    );
                }

                if !output.stderr.is_empty() {
                    println!("{}", str::from_utf8(&output.stderr)?.trim_end().red(),);
                }

                if !opts.continue_on_error {
                    exit(1);
                }
            }
            Err(e) => {
                term::erase_previous_line();
                term::print_error(&test.name);

                println!("{}", e);

                if !opts.continue_on_error {
                    exit(1);
                }
            }
        }
    }

    Ok(())
}
