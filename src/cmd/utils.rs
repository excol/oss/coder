use std::collections::HashMap;
use std::env;
use std::fs;
use std::path::Path;

use anyhow::Result;
use serde::{Deserialize, Serialize};

use crate::config::Script;
use crate::files::find;
use crate::git::list_files;

#[derive(Serialize, Deserialize)]
struct PackageJSON {
    scripts: Option<HashMap<String, String>>,
}

pub fn find_scripts() -> Result<Vec<Script>> {
    let mut scripts = vec![];
    let files = list_files()?;
    let package_json = find(files.iter(), "**/package.json")?;
    let makefile = find(files.iter(), "**/Makefile")?;

    for file in package_json {
        let contents = fs::read_to_string(&file.name)?;
        let package: PackageJSON = serde_json::from_str(&contents)?;

        if let Some(package_scripts) = package.scripts {
            let dirname = Path::new(&file.name)
                .parent()
                .and_then(|path| path.file_name())
                .and_then(|name| name.to_str());
            let mut workdir = env::current_dir()?;

            if let Some(dirname) = dirname {
                workdir.push(dirname);
            }

            for (name, _script) in package_scripts {
                scripts.push(Script {
                    path: match dirname {
                        Some(dirname) => vec!["npm".to_string(), dirname.to_string(), name.clone()],
                        None => vec!["npm".to_string(), name.clone()],
                    },
                    cmd: format!("npm run {}", name),
                    workdir: workdir.clone(),
                });
            }
        }
    }

    Ok(scripts)
}
