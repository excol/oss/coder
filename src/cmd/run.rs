use std::process::{exit, Command, Stdio};

use anyhow::Result;
use clap::Clap;
use colored::Colorize;

use crate::cmd::utils::find_scripts;
use crate::config::Config;
use crate::term;

#[derive(Clap)]
pub struct Opts {
    pub name: String,
}

pub fn run(config: Config, opts: Opts) -> Result<()> {
    let scripts = find_scripts()?;
    let scripts = scripts
        .iter()
        .filter(|script| script.path.join("/").contains(&opts.name))
        .collect::<Vec<_>>();

    if scripts.is_empty() {
        term::print_heading("run");
        term::print_error(format!("script '{}' not found", opts.name));

        exit(1);
    }

    if scripts.len() > 1 {
        term::print_heading("run");
        term::print_error(format!("script '{}' is ambiguous", opts.name));

        for script in scripts {
            let script_type = script.path.get(0).unwrap();
            let pathname = script.path[1..].join("/");

            term::print_list_item(format!(
                "{}{}{}",
                script_type.bold(),
                "::".bright_black(),
                pathname,
            ));
        }

        exit(1);
    }

    let script = scripts.get(0).unwrap();

    term::print_hint(format!(">> {}", script.cmd));

    Command::new("sh")
        .arg("-c")
        .arg(&script.cmd)
        .current_dir(&script.workdir)
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .spawn()?
        .wait()?;

    Ok(())
}
