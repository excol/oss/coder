use anyhow::Result;
use clap::Clap;
use colored::Colorize;

use crate::cmd::utils::find_scripts;
use crate::config::Config;
use crate::term;

#[derive(Clap)]
pub struct Opts {}

pub fn run(config: Config, opts: Opts) -> Result<()> {
    term::print_heading("scripts");

    let scripts = find_scripts()?;

    for script in scripts {
        let script_type = script.path.get(0).unwrap();
        let pathname = script.path[1..].join("/");

        term::print_list_item(format!(
            "{}{}{}",
            script_type.bold(),
            "::".bright_black(),
            pathname,
        ));
    }

    Ok(())
}
