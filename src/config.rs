use std::path::PathBuf;

use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
pub struct Condition {
    pub matches: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Test {
    pub name: String,
    pub cmd: String,
    pub when: Option<Condition>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Script {
    pub cmd: String,
    pub workdir: PathBuf,
    pub path: Vec<String>,
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Config {
    pub tests: Vec<Test>,
}
