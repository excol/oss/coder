use colored::Colorize;

pub fn erase_previous_line() {
    print!("\x1B[F\x1B[K");
}

pub fn print_success(message: impl AsRef<str>) {
    println!("{} {}", "✔".green(), message.as_ref());
}

pub fn print_error(message: impl AsRef<str>) {
    println!("{}{}", "❌".red(), message.as_ref());
}

pub fn print_heading(title: impl AsRef<str>) {
    println!(">> {}\n", title.as_ref().blue().bold());
}

pub fn print_list_item(item: impl AsRef<str>) {
    println!("• {}", item.as_ref());
}

pub fn print_hint(message: impl AsRef<str>) {
    println!("{}", message.as_ref().bright_black());
}
