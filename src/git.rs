use std::str;

use anyhow::Result;
use git2::Repository;

use crate::files::File;

pub fn list_staged_files() -> Result<Vec<File>> {
    let repo = Repository::init(".")?;
    let statuses = repo.statuses(None)?;

    Ok(statuses
        .iter()
        .filter(|entry| {
            let status = entry.status();

            status.is_index_new()
                || status.is_index_modified()
                //|| status.is_index_deleted()
                || status.is_index_renamed()
                || status.is_index_typechange()
        })
        .map(|entry| File {
            name: entry.path().unwrap().to_string(),
            git_oid: entry
                .head_to_index()
                .and_then(|diff| Some(diff.new_file().id()))
                .and_then(|id| Some(format!("{}", id))),
        })
        .collect::<Vec<_>>())
}

pub fn list_files() -> Result<Vec<File>> {
    let repo = Repository::init(".")?;
    let index = repo.index()?;

    Ok(index
        .iter()
        .map(|entry| File {
            name: str::from_utf8(&entry.path).unwrap().to_string(),
            git_oid: Some(format!("{}", entry.id)),
        })
        .collect::<Vec<_>>())
}
