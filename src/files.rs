use anyhow::Result;
use glob::Pattern;

#[derive(Clone)]
pub struct File {
    pub name: String,
    pub git_oid: Option<String>,
}

pub struct FileGlobIter<'i, I: Iterator<Item = &'i File>> {
    inner: I,
    pattern: Pattern,
}

impl<'i, I: Iterator<Item = &'i File>> FileGlobIter<'i, I> {
    pub fn new(inner: I, pattern: Pattern) -> Self {
        Self { inner, pattern }
    }
}

impl<'i, I: Iterator<Item = &'i File>> Iterator for FileGlobIter<'i, I> {
    type Item = &'i File;

    fn next(&mut self) -> Option<I::Item> {
        loop {
            let file = self.inner.next();

            if file.is_none() {
                return None;
            }

            let file = file.unwrap();

            if !self.pattern.matches(&file.name) {
                continue;
            }

            return Some(file);
        }
    }
}

pub fn find<'i, I>(files: I, patt: &str) -> Result<FileGlobIter<'i, I>>
where
    I: Iterator<Item = &'i File>,
{
    let pattern = Pattern::new(patt)?;

    Ok(FileGlobIter::new(files, pattern))
}
